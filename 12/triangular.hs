// TODO: Make this more efficient using prime factorization
divisors :: Int -> Int
divisors 0 = 0
divisors 1 = 0
divisors n = length [ x | x <- [1..n+1], (n `mod` x) == 0]

triangular_over :: Int -> Int -> Int -> Int
triangular_over x i accum 
                   | (divisors accum) > x = accum
                   | otherwise = triangular_over x (i+1) (i+accum)

triangulars :: Int -> Int
triangulars x = triangular_over x 0 0
