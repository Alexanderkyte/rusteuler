fn main(){
  let max = 4000000;
  let mut prev = 1;
  let mut curr = 1;
  let mut sum = 0;

  while curr < max {
    if curr % 2 == 0 {
      sum += curr;
    }

    let tmp = curr;
    curr = curr + prev;
    prev = tmp;
  }

  println(format!("The sum is {:d}", sum));
  return;
}
