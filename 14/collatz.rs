use std::hashmap::HashMap;


fn main(){

  let mut map = HashMap::<int, int>::new();
  let mut upper = 10;

  match ::std::from_str::from_str::<int>(::std::os::args()[0]) {
    Some(n) => upper = n,
    None => return ()
  }


  for i in range(1, upper) {
    let count = collatz_count(i, &map);
    map.insert(i, count);
  }

  let mut max = 0;
  let mut maxKey = 0;

  for (k, v) in map.iter() {
    if *v > max {
      maxKey = *k;
      max = *v;
    }    
  }
  println(format!("The longest chain belongs to {:d}", maxKey));

  return ()
}

fn collatz_count(x: int, map: &HashMap<int, int>) -> int {
  let mut i = x;
  let mut counter = 0;

  while !map.contains_key(&i) && i != 1 {
    i = collatz_next(i);
    counter += 1;
  }
  
  if i != 1 {
    counter += *map.get(&i);
  }
  
  counter
}

fn collatz_next(x: int) -> int {
  if x % 2 == 0 {
    x / 2
  } else {
    3*x + 1
  }
}
