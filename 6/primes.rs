fn main(){

  let mut prime_vector  = ~[2, 3];
  let mut i = 3;  
  let mut s = 2;

  while s < 1001 {
    let mut skip = false;
    i += 1;
    for n in prime_vector.iter() {
      if i % *n == 0 {
        skip = true;
      }
    }
    if skip {
      continue;
    } else {
      prime_vector.push(i);
      s += 1;
    }
  }

  println(format!("The primes are {:?}", prime_vector));
  return;
}
